using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCell : MonoBehaviour
{
    public float speed;
    public float scal;
    void Start()
    {
        if (!gameObject.CompareTag("Boss"))
        {
           speed = Random.Range(0.5f, 1.5f);
           scal = Random.Range(0.5f, 5f);
           this.transform.localScale = new Vector3(scal, scal, 1f); 
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChaseObject(Vector3 objectPOS)
    {
        //Debug.Log("chase");
        var transformPosition = this.transform.position;
        
        if (this.transform.position.x > objectPOS.x)
            transformPosition.x -= speed * Time.deltaTime;
        if (this.transform.position.x < objectPOS.x)
            transformPosition.x += speed * Time.deltaTime;
        
        if (this.transform.position.y > objectPOS.y)
            transformPosition.y -= speed * Time.deltaTime;
        if (this.transform.position.y < objectPOS.y)
            transformPosition.y += speed * Time.deltaTime;

        
        this.transform.position = transformPosition;

    }
    
    public void EvadeObject(Vector3 objectPOS)
    {
        //Debug.Log("Evase");
        var transformPosition = this.transform.position;
        
        if (this.transform.position.x > objectPOS.x)
            transformPosition.x += speed * Time.deltaTime;
        if (this.transform.position.x < objectPOS.x)
            transformPosition.x -= speed * Time.deltaTime;
        
        if (this.transform.position.y > objectPOS.y)
            transformPosition.y += speed * Time.deltaTime;
        if (this.transform.position.y < objectPOS.y)
            transformPosition.y -= speed * Time.deltaTime;

        
        this.transform.position = transformPosition;

    }
}
