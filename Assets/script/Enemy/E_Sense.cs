using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Sense : MonoBehaviour
{
    public GameObject MainE_Cell;
    public float cellSenseRaius;
    void Start()
    {
        var CellTrigger = this.GetComponent<CircleCollider2D>();
        CellTrigger.radius = cellSenseRaius;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        var MainE_CellControll = MainE_Cell.GetComponent<EnemyCell>();
        var OtherGameObject = other.gameObject;

        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();
        
        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.OtherCell:
                    var otherGameObjectMain = OtherGameObject.GetComponent<CellTrigger>().MainCell;
                    
                    if (otherGameObjectMain.transform.localScale.x < MainE_Cell.transform.localScale.x)
                        MainE_CellControll.ChaseObject(OtherGameObject.transform.position);
                    if (otherGameObjectMain.transform.localScale.x > MainE_Cell.transform.localScale.x)
                        MainE_CellControll.EvadeObject(OtherGameObject.transform.position);
                    break;
                
                case ObjectType.MainCell:
                    var otherGameObjectMain_MainCell = OtherGameObject.GetComponent<CellTrigger>().MainCell;
                    if (otherGameObjectMain_MainCell.transform.localScale.x < MainE_Cell.transform.localScale.x)
                        MainE_CellControll.ChaseObject(OtherGameObject.transform.position);
                    if (otherGameObjectMain_MainCell.transform.localScale.x > MainE_Cell.transform.localScale.x)
                        MainE_CellControll.EvadeObject(OtherGameObject.transform.position);
                    break;
                
                case ObjectType.ChildCell:
                    var otherGameObjectMain_ChildCell = OtherGameObject.GetComponent<CellTrigger>().MainCell;
                    
                    if (otherGameObjectMain_ChildCell.transform.localScale.x < MainE_Cell.transform.localScale.x)
                        MainE_CellControll.ChaseObject(OtherGameObject.transform.position);
                    if (otherGameObjectMain_ChildCell.transform.localScale.x > MainE_Cell.transform.localScale.x)
                        MainE_CellControll.EvadeObject(OtherGameObject.transform.position);
                    break;
            }
            
        }
    }
    
    
}
