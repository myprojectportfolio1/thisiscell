using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_Trigger : MonoBehaviour
{
    public GameObject MainCell;
    
    public AudioClip s_eat;
    public AudioClip s_copy;
    public AudioClip s_die;
    public AudioSource soundManager;

    private void Start()
    {
        soundManager = this.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject OtherGameObject = other.gameObject;
        var OtherGameObject_Main = OtherGameObject.GetComponent<CellTrigger>();
        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();
        
        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.Trap:
                    SoundDie();
                    Destroy(MainCell.gameObject);
                    break;
                case ObjectType.SuicideTrap:
                    SoundDie();
                    Destroy(MainCell.gameObject);
                    Destroy(other.gameObject);
                    break;
                case ObjectType.CellFood:
                    SoundEat();
                    MainCell.transform.localScale += other.gameObject.transform.localScale;
                    Destroy(other.gameObject);
                    break;
                case ObjectType.CellDuplicate:
                    SoundCopy();
                    var newCell = Instantiate(MainCell, other.gameObject.transform.position, new Quaternion());
                    var newCellControll = newCell.GetComponent<CellControll>();
                    newCell.transform.localScale = MainCell.transform.localScale;
                    Destroy(other.gameObject);
                    break;
                case ObjectType.OtherCell:
                    if (OtherGameObject_Main.MainCell.transform.localScale.x > MainCell.transform.localScale.x)
                    {
                        SoundDie();
                        OtherGameObject_Main.MainCell.transform.localScale += MainCell.transform.localScale;
                        Destroy(MainCell.gameObject);
                    }
                    if (OtherGameObject_Main.MainCell.transform.localScale.x < MainCell.transform.localScale.x)
                    {
                        SoundEat();
                        OtherGameObject_Main.ChangCellType();
                        MainCell.transform.localScale += OtherGameObject_Main.MainCell.transform.localScale;
                        Destroy(OtherGameObject_Main.MainCell.gameObject);
                    }
                    break;
                case ObjectType.MainCell:
                    if (OtherGameObject_Main.MainCell.transform.localScale.x > MainCell.transform.localScale.x)
                    {
                        SoundDie();
                        OtherGameObject_Main.MainCell.transform.localScale += MainCell.transform.localScale;
                        Destroy(MainCell.gameObject);
                    }

                    if (OtherGameObject_Main.MainCell.transform.localScale.x < MainCell.transform.localScale.x)
                    {
                        SoundEat();
                        OtherGameObject_Main.ChangCellType();
                        MainCell.transform.localScale += OtherGameObject_Main.MainCell.transform.localScale;
                        Destroy(OtherGameObject_Main.MainCell.gameObject);
                    }
                    break;
                case ObjectType.ChildCell:
                    if (OtherGameObject_Main.MainCell.transform.localScale.x > MainCell.transform.localScale.x)
                    {
                        SoundDie();
                        OtherGameObject_Main.MainCell.transform.localScale += MainCell.transform.localScale;
                        Destroy(MainCell.gameObject);
                    }

                    if (OtherGameObject_Main.MainCell.transform.localScale.x < MainCell.transform.localScale.x)
                    {
                        SoundEat();
                        OtherGameObject_Main.ChangCellType();
                        MainCell.transform.localScale += OtherGameObject_Main.MainCell.transform.localScale;
                        Destroy(OtherGameObject_Main.MainCell.gameObject);
                    }
                    break;
            }
        }
    }
    
    void SoundEat()
    {
        soundManager.volume = 0.5f;
        soundManager.clip = s_eat;
        soundManager.Play();
    }

    void SoundDie()
    {
        soundManager.volume = 0.5f;
        soundManager.clip = s_die;
        soundManager.Play();
    }

    void SoundCopy()
    {
        soundManager.clip = s_copy;
        soundManager.volume = 0.1f; 
        soundManager.Play();
    }
    
}
