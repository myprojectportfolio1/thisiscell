using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject startUI;
    public GameObject endUI;

    public GameObject player;
    public Vector3 m_position;
    public float cameraspeed;

    private Vector3 d_etween;
    private float d_X;
    private float d_Y;
    private float d_Sum;
    private Vector3 d_Move;
    private Vector3 s_Direction;
    void Start()
    {
        this.gameObject.transform.position = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        m_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_position.z = this.transform.position.z;
        this.gameObject.transform.position = m_position * Time.deltaTime;
        */
        MoveToMousePOS();
    }
    
    void MoveToMousePOS()
    {
        m_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_position.z = this.transform.position.z;
            
        d_etween = m_position - this.transform.position;
        d_Move = Vector3.Normalize(d_etween);
        
        s_Direction = d_Move;
        
        #region FindSpeedWithDistance

        if (d_etween.x < 0)
            d_X = d_etween.x * -1;
        else
            d_X = d_etween.x;
        ////////////////////////////////////////////
        if (d_etween.y < 0)
            d_Y = d_etween.y * -1;
        else
            d_Y = d_etween.y;

        d_Sum = d_X + d_Y;
        #endregion

        s_Direction *= cameraspeed + d_Sum;

        this.transform.position += s_Direction * Time.deltaTime;
        
    }

    public void StartGame()
    {
        startUI.SetActive(false);
    }

    public void EndGame()
    {
        endUI.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("MainMap", LoadSceneMode.Single);
    }
}
