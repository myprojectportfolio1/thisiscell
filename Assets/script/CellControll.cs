using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;


public class CellControll : MonoBehaviour
{
    public Vector3 mouseWorldPosition;
    public Vector3 directionToMove;
    public Vector3 distanceBetween;
    public Vector3 speedDirection;

    public float speedMultiply = 0.1f;
    public GameObject ChildCell;

    private float distanceX;
    private float distanceY;
    private float distanceSum;

    public bool Eat;
    private bool Root;
    public int RootCount;
    public int RootLimit;

    public List<GameObject> trigerObject;
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        MoveToMousePOS();
        MouseAction();
        
    }

    void MoveToMousePOS()
    {
        mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.z = this.transform.position.z;
            
        distanceBetween = mouseWorldPosition - this.transform.position;
        directionToMove = Vector3.Normalize(distanceBetween);
        
        speedDirection = directionToMove;
        
        #region FindSpeedWithDistance

        if (distanceBetween.x < 0)
            distanceX = distanceBetween.x * -1;
        else
            distanceX = distanceBetween.x;
        ////////////////////////////////////////////
        if (distanceBetween.y < 0)
            distanceY = distanceBetween.y * -1;
        else
            distanceY = distanceBetween.y;

        distanceSum = distanceX + distanceY;
        #endregion

        speedDirection *= speedMultiply + distanceSum;

        this.transform.position += speedDirection * Time.deltaTime;
        
    }

    void MouseAction()
    {
        if (Input.GetMouseButton(1))
            Eat = true;
        if (!Input.GetMouseButton(1))
            Eat = false;

        if (Input.GetMouseButtonDown(0) && RootCount <= RootLimit)
        {
            RootCell();
        }
            

    }

    void RootCell()
    {
        RootCount++;
        this.transform.localScale /= 2;
        Vector3 bornPos = new Vector3(
            this.transform.position.x + Random.Range(-2, 2),
            this.transform.position.y + Random.Range(-2, 2),
            this.transform.position.z);
        var newCell = Instantiate(ChildCell, directionToMove + this.transform.position, new Quaternion());
        var newCellControll = newCell.GetComponent<CellControll>();
        var newCellType = newCell.GetComponentInChildren<ObjectType_Identities>();
        newCellType.Type = ObjectType.ChildCell;
        newCellControll.RootCount = this.RootCount;
        newCellControll.RootLimit = this.RootLimit;
        newCell.transform.localScale = this.transform.localScale;
    }

    /*
    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject OtherGameObject = other.gameObject;
        trigerObject.Add(OtherGameObject);
        
        ObjectType_Identities ThisType = this.GetComponent<ObjectType_Identities>();
        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();
        
        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.Trap:
                    //Destroy(this.gameObject);
                    break;
                case ObjectType.SuicideTrap:
                    break;
                case ObjectType.CellFood:
                    break;
                case ObjectType.CellDuplicate:
                    break;
            }
        }
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        GameObject OtherGameObject = other.gameObject;
        trigerObject.Remove(OtherGameObject);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        ObjectType_Identities ThisType = this.GetComponent<ObjectType_Identities>();
        GameObject OtherGameObject = other.gameObject;
        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();
        
        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.ChildCell:
                    Debug.Log("EAT_ChildCell");
                    if(Eat)
                        if (ThisType.Type == ObjectType.MainCell)
                        {
                            this.transform.localScale += other.transform.localScale;
                            Destroy(OtherGameObject);
                            RootCount = 0;
                        }
                    break;
                case ObjectType.Trap:
                    Destroy(this.gameObject);
                    break;
                case ObjectType.SuicideTrap:
                    break;
                case ObjectType.CellFood:
                    break;
                case ObjectType.CellDuplicate:
                    break;
            }
        }
    }
    */
}
