using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class CellTrigger : MonoBehaviour
{
    public GameObject MainCell;
    public List<GameObject> trigerObject;
    public AudioClip s_eat;
    public AudioClip s_copy;
    public AudioClip s_die;
    public AudioSource soundManager;

    private void Start()
    {
        var MainCellControll = MainCell.GetComponent<CellControll>();
        soundManager = this.GetComponent<AudioSource>();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var MainCellControll = MainCell.GetComponent<CellControll>();
        GameObject OtherGameObject = other.gameObject;
        trigerObject.Remove(OtherGameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();
        ObjectType_Identities ThisType = this.GetComponent<ObjectType_Identities>();
        
        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.ChildCell:
                    GameObject OtherGameObject = other.GetComponent<CellTrigger>().MainCell;
                    if (ThisType.Type == ObjectType.MainCell)
                        trigerObject.Add(other.gameObject);
                    break;
                case ObjectType.Trap:
                    SoundDie();
                    ChangCellType();
                    Destroy(MainCell);
                    break;
                case ObjectType.SuicideTrap:
                    SoundDie();
                    ChangCellType();
                    Destroy(MainCell);
                    Destroy(other.gameObject);
                    break;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        var MainCellControll = MainCell.GetComponent<CellControll>();
        ObjectType_Identities ThisType = this.GetComponent<ObjectType_Identities>();
        ObjectType_Identities OtherType = other.GetComponent<ObjectType_Identities>();

        if (OtherType != null)
        {
            switch (OtherType.Type)
            {
                case ObjectType.ChildCell:
                    GameObject OtherGameObject = other.GetComponent<CellTrigger>().MainCell;
                    if (ThisType.Type == ObjectType.MainCell)
                    {
                        if(MainCellControll.Eat)
                        {
                            SoundEat();
                            MainCell.transform.localScale += OtherGameObject.transform.localScale;
                            Destroy(OtherGameObject);
                            MainCellControll.RootCount = 0;
                        }
                    }
                    
                    break;
                case ObjectType.CellFood:
                    SoundEat();
                    MainCell.transform.localScale += other.gameObject.transform.localScale;
                    Destroy(other.gameObject);
                    break;
                case ObjectType.CellDuplicate:
                    SoundCopy();
                    var newCell = Instantiate(MainCellControll.ChildCell, MainCellControll.directionToMove + MainCell.transform.position, new Quaternion());
                    var newCellControll = newCell.GetComponent<CellControll>();
                    var newCellType = newCell.GetComponentInChildren<ObjectType_Identities>();
                    newCellType.Type = ObjectType.ChildCell;
                    newCellControll.RootCount = MainCellControll.RootCount;
                    newCellControll.RootLimit = MainCellControll.RootLimit;
                    newCell.transform.localScale = MainCellControll.transform.localScale;
                    Destroy(other.gameObject);
                    break;
            }
        }
    }

    public void ChangCellType()
    {
        ObjectType_Identities ThisType = this.GetComponent<ObjectType_Identities>();
        if (ThisType.Type == ObjectType.MainCell)
        {
            var MainColor = MainCell.GetComponent<SpriteRenderer>().color;
            if (trigerObject.Count > 0)
            {
                var cell = trigerObject[Random.Range(0, trigerObject.Count)];
                var new_Identiti = cell.GetComponent<ObjectType_Identities>();
                new_Identiti.Type = ObjectType.MainCell;
                var cellColor = cell.GetComponentInParent<SpriteRenderer>();
                cellColor.color = MainColor;
                /*
                foreach (var Cell in trigerObject)
                {
                    if (Cell.GetComponent<ObjectType_Identities>() && Cell.GetComponent<CellTrigger>())
                    {
                       
                        var new_Identiti = Cell.GetComponent<ObjectType_Identities>();
                        new_Identiti.Type = ObjectType.MainCell;
                    }
                }
                */
            }
            else
            {
                Debug.Log("LOSE");
                var endtriger = GameObject.Find("GameManager").GetComponent<GameManager>();
                endtriger.EndGame();
            }
        }
    }

    void SoundEat()
    {
        soundManager.volume = 0.5f;
        soundManager.clip = s_eat;
        soundManager.Play();
    }

    void SoundDie()
    {
        soundManager.volume = 0.5f;
        soundManager.clip = s_die;
        soundManager.Play();
    }

    void SoundCopy()
    {
        soundManager.clip = s_copy;
        soundManager.volume = 0.001f; 
        soundManager.Play();
    }
}
